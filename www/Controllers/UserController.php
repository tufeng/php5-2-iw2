<?php

namespace App\controllers;

use App\Core\Exceptions\NotFoundException;
use App\core\View;
use App\models\Users;
use App\core\Validator;

class UserController
{
    public function defaultAction()
    {
        echo "Action default dans le controller user";
    }

    public function addAction()
    {
        echo "Action add dans le controller user";
    }
    

    public function loginAction()
    {
        $myView = new View("login", "account");
    }

    public function getAction($params)
    {
        $userManager = new Users();

        $user = $userManager->find($params['id']);

        if(!$user) {
            throw new NotFoundException("User not found");
        }

        echo json_encode($user);
        $users = $userManager->findAll();

        $partialUsers = $userManager->findBy(['firstname' => 'Yves']);

        $count = $userManager->count(['firstname' => 'Yves']);

        $userManager->delete(2);

        echo "get user";
    }

    public function registerAction()
    {

        $configForm  = Users::getRegisterForm();

        if( $_SERVER["REQUEST_METHOD"] == "POST"){

            //Vérification des champs
            $errors = Validator::formValidate( $configForm, $_POST );
            print_r($errors);

        }


        $user = new users();
        $user->setId(1);
        $user->setFirstname("Yves");
        $user->save();


        $myView = new View("register", "account");
        $myView->assign("configForm", $configForm);
    }





    public function forgotPwdAction()
    {
        $myView = new View("forgotPwd", "account");
    }
}
