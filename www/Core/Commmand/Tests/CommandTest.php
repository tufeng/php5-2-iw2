<?php declare(strict_types=1);

namespace DesignPatterns\Behavioral\Command\Tests;

use App\Core\Command\HelloCommand;
use App\Core\Command\Invoker;
use App\Core\Command\Receiver;
use PHPUnit\Framework\TestCase;

class CommandTest extends TestCase
{
    public function testInvocation()
    {
        $invoker = new Invoker();
        $receiver = new Receiver();

        $invoker->setCommand(new HelloCommand($receiver));
        $invoker->run();
        $this->assertSame('Hello World', $receiver->getOutput());
    }
}