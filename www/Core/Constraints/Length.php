<?php

namespace App\Core\Constraints;

class Length implements ConstraintInterface
{
    protected int $min;
    protected int $max;
    protected string $minMessage;
    protected string $maxMessage;
    protected $errors = [];

    public function __construct(int $min, int $max, string $minMessage = null, string $maxMessage = null)
    {

    }

    public function isValid(string $value): bool
    {
        // TODO: Implement isValid() method.
    }

    public function getErrors(string $value): array
    {
        // TODO: Implement getErrors() method.
    }
}