<?php

namespace App\core;

use PDO;
use Exception;

class DB
{
    private $table;
    private $pdo;

    //SINGLETON
    public function __construct()
    {
        try {
            $this->pdo = new PDO(DRIVER_DB.":host=".HOST_DB.";dbname=".NAME_DB, USER_DB, PWD_DB);
        } catch (Exception $e) {
            die("Erreur SQL : ".$e->getMessage());
        }

        //A completer
        $this->table = PREFIXE_DB.get_called_class();
    }

    //correction save
    public function save($objectToSave) {
        $objectArray = $objectToSave->__toArray();
        $columnData = array_values($objectArray);
        $colums = array_keys($objectArray);
        $params = array_combine(
            array_map(function ($k){
                return ':'.$k;
            },array_keys($objectArray)),$objectArray
        );

        if (!is_numeric($objectArray->getId())) {
            //INSERT
            $sql = "INSERT INTO ".$this->table." (".implode(",", $colums).") VALUES (:".implode(",:", $colums).");";
        } else {
            //UPDATE
            foreach ($colums as $colum) {
                $sqlUpdate[] =  $colum."=:".$colum;
            }
            $sql = "UPDATE ".$this->table." SET ".implode(",", $sqlUpdate)." WHERE id=:id;";
        }

        $this->sql($sql,$params);
    }

//    public function save()
//    {
//        $objectVars = get_object_vars($this);
//        $classVars = get_class_vars(get_class());
//        $columsData = array_diff_key($objectVars, $classVars);
//        $colums = array_keys($columsData);
//
//        if (!is_numeric($this->id)) {
//            //INSERT
//            $sql = "INSERT INTO ".$this->table." (".implode(",", $colums).") VALUES (:".implode(",:", $colums).");";
//        } else {
//            //UPDATE
//            foreach ($colums as $colum) {
//                $sqlUpdate[] =  $colum."=:".$colum;
//            }
//            $sql = "UPDATE ".$this->table." SET ".implode(",", $sqlUpdate)." WHERE id=:id;";
//        }
//
//        $queryPrepared = $this->pdo->prepare($sql);
//        $queryPrepared->execute($columsData);
//    }

    public function find(int $id) :?\App\models\Model
    {
        $sql = "SELECT * FROM ".$this->table." WHERE id= :id";
        $result = $this->sql($sql,[':id' => $id]);

        $row = $result->fetch();

        if($row){
            $object = new $this->class();
            return $object->hydrate($row);
        } else {
            return null;
        }
    }

    public function findBy(array $params, array $order = null): ?array
    {
        $result = array();

        $sql = "SELECT * FROM ". $this->table ." WHERE ";

        foreach ($params as $key => $value) {
            if(is_string($value)){
                $comparator = 'LIKE';
            } else {
                $comparator = '=';
            }
            $sql .= $key.$comparator.':'.$key.' AND';

            $params[":".$key] = $value;
            unset($params[$key]);
        }

        $sql = rtrim($sql, 'and');

        if($order) {
            $sql .= "ORDER BY ". key($order). " ". $order[key($order)];
        }

        $result = $this->sql($sql,$params);
        $rows = $result->fetchAll();

        foreach ($rows as $row) {
            $object = new $this->class();
            array_push($results, $object->hydrate($row));
        }

        return $results;
    }

    public function count(array $params): int
    {
        $sql = "SELECT count(*) FROM ". $this->table." WHERE ";

        foreach ($params as $key => $value) {
            if(is_string($value)){
                $comparator = 'LIKE';
            }else {
                $comparator = '=';
            }

            $sql .= " ".$key.$comparator.":".$key." and";

            $params[":".$key] = $value;
            unset($params[$key]);
        }

        $sql = rtrim($sql, 'and');

        $result = $this->sql($sql,$params);

        return $result->fetchColumn();
    }

    public function findAll()
    {
        $sql = "SELECT * FROM ".$this->table;
        $result = $this->sql($sql);
        $rows = $result->fetchAll();

        $results = array();

        foreach ($rows as $row){

            $object = new $this->class();
            array_push($results, $object->hydate($row));
        }

        return $results;
    }

    public function delete(int $id):bool
    {
        $sql = "DELETE FROM ". $this->table ."WHERE id=:id";

        $result = $this->sql($sql, [':id' => $id]);

        return true;
    }
}
