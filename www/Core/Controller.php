<?php


namespace App\core;


class Controller
{
    public $observers;
    public $event;

    public function __construct()
    {
        $this->event = new ControllerEvent();
        $this->observers = new \SplObjectStorage();
    }

    public function createForm(string $class, Model &$model = null): Form
    {
        $form = new $class;
        $form->configureOptions();
        $form->buildForm(new \FormBuilder());

        if($model){
            $form->setModel($model);
            $form->associativeValue();
        }

        return $form;
    }

    public function redirectTo(string $contorller,$action) {

    }
}